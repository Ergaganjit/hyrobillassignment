public class internet extends bill implements IDisplay {
	String providerName;
	double internetGBused;

	public internet(int billId, String billDate, String billType, double totalbillAmount, String providerName,
			double internetGBused) {
		super(billId, billDate, billType, totalbillAmount);
		// TODO Auto-generated constructor stub
		this.providerName = providerName;
		this.internetGBused = internetGBused;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public double getInternetGBused() {
		return internetGBused;
	}

	public void setInternetGBused(double internetGBused) {
		this.internetGBused = internetGBused;
	}

	@Override
	public void Display() {
		// TODO Auto-generated method stub
		super.Display();
		System.out.println("Bill Id: " + this.billId + "\n " + " Bill Date: " + this.billDate + "\n " + " Bill Type: "
				+ this.billType + "\n " + " Bill Amount: $" + this.totalbillAmount + "\n " + " Provider Name: "
				+ this.providerName + "\n " + " Internet Usage: " + this.internetGBused + "GB");
		System.out.println("*************************************************************");

	}

}
