public class Mobile extends bill implements IDisplay {

	String manufacturerName;
	String planName;
	double mobileNumber;
	double internetGBused;
	double minuteUsed;

	public Mobile(int billId, String billDate, String billType, double totalbillAmount, String manufacturerName,
			String planNme, double mobileNumber, double internetGBused, double minuteUsed) {
		super(billId, billDate, billType, totalbillAmount);
		// TODO Auto-generated constructor stub
		this.manufacturerName = manufacturerName;
		this.planName = planNme;
		this.mobileNumber = mobileNumber;
		this.internetGBused = internetGBused;
		this.minuteUsed = minuteUsed;
	}

	public String getManufacturerName() {
		return manufacturerName;
	}

	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public double getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(double mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public double getInternetGBused() {
		return internetGBused;
	}

	public void setInternetGBused(double internetGBused) {
		this.internetGBused = internetGBused;
	}

	public double getMinuteUsed() {
		return minuteUsed;
	}

	public void setMinuteUsed(double minuteUsed) {
		this.minuteUsed = minuteUsed;
	}

	@Override
	public void Display() {
		// TODO Auto-generated method stub
		super.Display();
		System.out.println("Bill Id: " + this.billId+"\n " + " Bill Date: " + this.billDate+"\n " + " Bill Type: " + this.billType+"\n "
				+ " Bill Amount: $" + this.totalbillAmount+"\n " + " Manufacturer Name: " + this.manufacturerName+"\n "
				+ " Plan Name: " + this.planName +"\n "+ " Mobile Number: +" + this.mobileNumber+"\n " + " Internet Usage: "
				+ this.internetGBused+"\n " + " Minutes Usage: " + this.minuteUsed);
		System.out.println("*************************************************************");

	}

}