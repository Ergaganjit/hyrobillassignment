
import java.util.ArrayList;

public class Customer implements IDisplay {
	int customerId;
	String firstName;
	String lastName;
	String fullName;
	String emailId;
	ArrayList<bill>billList = new ArrayList<bill>();
	
	public Customer(int cutomerId,String firstName, String lastName, String fullName, String emailId) {
	this.customerId=cutomerId;
	this.firstName=firstName;
	this.lastName=lastName;
	this.fullName=fullName;
	this.emailId=emailId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public double getTotalAmounttopay() {
		double tot = 0;
		
		for (int i = 0; i < billList.size(); i++) {
			tot += billList.get(i).getTotalbillAmount();
		}
		return tot;
		
	}

	

	public ArrayList<bill> getBillList() {
		return billList;
	}

	public void setBillList(ArrayList<bill> billList) {
		this.billList = billList;
	}
public void billList(bill b) {
	billList.add(b);
	
}
	@Override
	public void Display() {
		// TODO Auto-generated method stub
		System.out.println("Customer Id: "+this.customerId+"\n "+" Customer Full Name: "+this.fullName+"\n "+" Customer EmailId: "+this.emailId);
		System.out.println("*************************************************************");
		for (int i = 0; i < billList.size(); i++) {
		billList.get(i).Display();
		}
		System.out.println("The Total Amount to Pay:$ "+this.getTotalAmounttopay());
		System.out.println("*************************************************************");
	
	}
	
	
}
