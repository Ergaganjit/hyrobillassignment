
public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Customer c1 = new Customer(1, "Peter", "Sigurdson", "Peter Sigurdson", "peter@gmail.com");
		bill b1 = new Hydro(1, "Wednesday, 19 June, 2019", "Hydro", 45.35, "HydroOne", 29);
		c1.billList(b1);
		bill b2 = new internet(2, "Wednesday, 19 June, 2019", "Internet", 56.50, "Rogers", 500);
		c1.billList(b2);
		c1.Display();

		Customer c2 = new Customer(2, "Emad", "Nasrallah", "Emad Nasrallah", "dr.emad@gmail.com");
		bill b3 = new Hydro(1, "Wednesday, 19 June, 2019", "Hydro", 45.35, "Enbridge", 29);
		c2.billList(b3);
		bill b4 = new internet(2, "Wednesday, 19 June, 2019", "Internet", 56.50, "Rogers", 500);
		c2.billList(b4);
		bill b5 = new Mobile(3, "Thursday, 24 January, 2019", "Mobile", 250.69, "Galaxy Samsung Inc.",
				"Prepaid Talk + Text plan", 12345678, 5, 356);
		c2.billList(b5);
		bill b6 = new Mobile(4, "Wednesday, 19 June, 2019", "Mobile", 300.78, "Apple Inc. iPhone X MAX+",
				"LTE+3G 9.5GB Promo Plan", 5678947, 4, 230);
		c2.billList(b6);
		c2.Display();
		Customer c3 = new Customer(3, "Mohammad", "Kiani", "Muhammad Kiani", "mkiani@gmail.com");
		bill b7 = new Mobile(4, "Wednesday, 19 June, 2019", "Mobile", 300.78, "Apple Inc. iPhone X MAX+",
				"LTE+3G 9.5GB Promo Plan", 5678947, 4, 230);
		c2.billList(b7);
	}

}